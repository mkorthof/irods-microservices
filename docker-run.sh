#!/bin/bash

#################################################################################
# Run local container and/or simulate GitLab CI stages
# Uses variables from .gitlab-ci.yml, needs 'yq' installed
# Examples: 'IT=1 ./docker-run.sh' or 'STAGE=build ./docker-run.sh'
# ( first run ./docker-buid.sh to create the image )
################################################################################

# Example repositories, uncomment one to override gitlab-ci and env var
#   SRC_REPO="https://gitlab.com/mkorthof/irods-example-microservices.git"
#   SRC_REPO="https://git.wageningenur.nl/rdm-infrastructure/irods-microservices"
#   SRC_REPO="https://github.com/chStaiger/wur-microservices"
#   SRC_REPO="https://github.com/UtrechtUniversity/irods-uu-microservices"
#   SRC_REPO="https://github.com/MaastrichtUniversity/irods-microservices" # 4.2.6
#   IRODS_VERSION="4.2.11"

# Docker run variables
NAME="irods"
CT_HOSTNAME="icat.irods"

# GitLab variables
#CI_PROJECT_ID="123"
#TOKEN="abc"   # Use PAT instead of $CI_TOKEN (scope: api, write_repository)


SCRIPT="./$(basename "$0")"

if echo "$@" | grep -Eq -- "(-h| help)"; then
  cat <<-_EOF_

  Run local container

  Simulate GitLab CI stages:
    STAGE="build|test|publish"

  Instead of irods, start interactive shell with tty in container:
    IT=1
  Start irods, then open interactive shell:
    IT=2

  Env variables:
    Defaults from .gitlab-ci are used, set var to override e.g.
    SRC_REPO="https://github.com/myname/my-microservices"
    DOCKER_IMAGE="centos7_irods:4.2.11"

  Examples:
    STAGE=build ${SCRIPT}
    IT=1 DOCKER_IMAGE="centos7_irods:4.2.9" ${SCRIPT}

  Without IT or STAGE set, the script will start irods in background

_EOF_
  exit 0
fi

#set -x

func_source_vars() {
  # shellcheck disable=SC1090
  source ./.vars.$$
  if [ "$DEBUG" -gt 1 ]; then
    cat .vars.$$
  fi
  rm .vars.$$
}

# Make sure temp .vars.XXX get cleaned up
trap 'rm -f .vars.$$' EXIT HUP INT TERM
./gitlab-ci-vars.sh --global > .vars.$$ && func_source_vars

if [ "${STAGE}" = "build" ] && [ -z "${SRC_REPO}" ]; then
  echo "ERROR: missing 'SRC_REPO'"
  exit 1
fi

# remove any leading slash
DOCKER_IMAGE="${DOCKER_IMAGE#/}"

if [ -z "${DOCKER_IMAGE}" ]; then
  #DOCKER_IMAGE="centos7_irods_4.3.0:slim"
  echo "ERROR: missing 'DOCKER_IMAGE'"
  exit 1
fi

if [ -z "${CT_HOSTNAME}" ]; then
  CT_HOSTNAME="icat.irods"
fi

# rpm-package
set_rpm_file() {
  i=0
  for f in ./*.rpm; do
    test -s "$f" && i=$((i+1))
  done
  if [ "${i:-0}" -eq 1 ]; then
    RPM="$(basename "$f")"
  else
    echo "ERROR: $i rpm(s) found"
    exit 1
  fi
}

# repository-dir
set_repo_dir() {
  _tmp="${SRC_REPO##*\/}"
  REPO_DIR="${_tmp%%.git}"
}

# irods-version
#set_irods_ver() {
#  _tmp="${DOCKER_IMAGE##*:}"
#  IRODS_VERSION="${_tmp%%-slim}"
#}

if [ "${DEBUG:-0}" -eq 1 ]; then
  echo "DEBUG: CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE"
  echo "DEBUG: DOCKER_IMAGE=$DOCKER_IMAGE"
  echo "DEBUG: SRC_REPO=$SRC_REPO"
  #exit
fi

# check docker logs to verify irods is started
while_irods_wait() {
  i=1; m=25; s=5
  echo "Wait for irods server, get status from docker logs ... "
  while ! docker logs "$1" 2>/dev/null | grep -qa 'MsgHeader_PI'; do
    if [ "$i" -gt $m ]; then
      echo "ERROR: looks like irods is unavailable, give up and show logs.."
      docker logs "$1"
      break
    fi
    echo "Fetching logs every ${s}s to check status, attempts left: $((m-i))"
    sleep $s
    i=$((i+1))
  done
}

if [ "${IT:-0}" -eq 1 ]; then
  echo "* Starting bash shell in container \"irods-it.$$\" instead of irods ... "
  echo "  Using volume \"$PWD\" as /host"
  docker run "$@" \
    --rm \
    --interactive \
    --tty \
    --name "irods-it.$$" \
    --hostname "${CT_HOSTNAME}" \
    --entrypoint '/bin/bash' \
    --volume="$PWD":/host \
    --workdir /host \
    "${DOCKER_IMAGE}"
elif [ "${IT:-0}" -eq 2 ]; then
  echo "* Starting irods and postgres container \"irods-it.$$\" and running bash shell ... "
  echo "* After its done setting up irods, you might have to press [ENTER] to get a prompt ... "
  echo "  Using volume \"$PWD\" as /host"
  echo
  docker run "$@" \
    --rm \
    --interactive \
    --tty \
    --name "irods-it.$$" \
    --hostname "${CT_HOSTNAME}" \
    --volume="$PWD":/host \
    --workdir /host \
    "${DOCKER_IMAGE}" \
    bash
else
  case $STAGE in
    interactive)
      echo "Please use: 'IT=1 ${SCRIPT}' (or try '-h' for help)"
      exit
     ;;
    build)
      set_repo_dir
      # shellcheck disable=SC1004
      docker run "$@" \
        --rm \
        --env IRODS_VERSION="${IRODS_VERSION}" \
        --env CI_DEBUG_TRACE="${CI_DEBUG_TRACE}" \
        --env VERBOSE=1 \
        --entrypoint '/bin/sh' \
        --volume="$PWD":/builds \
        --workdir /builds \
        "${DOCKER_IMAGE}" \
        -c 'git clone '"${SRC_REPO}"'; \
            ./patch.sh '"${REPO_DIR}"' '"${CI_SERVER_URL}"'; \
            ( cd '"${REPO_DIR}"' || exit 1;
              ../ci-build.sh && mv ./*.rpm .. || exit 1 )'
      ;;
    test)
      set_rpm_file
      set_repo_dir
      CI_COMMIT_SHORT_SHA="$(date +%H%M%S)"
      printf "Starting irods container ... "
      docker run "$@" \
        --rm \
        --detach \
        --hostname "${CT_HOSTNAME}" \
        --name "irods-test-${CI_COMMIT_SHORT_SHA}" \
        --env IRODS_VERSION="${IRODS_VERSION}" \
        --env REPO_DIR="${REPO_DIR}" \
        --env PG_HOST="${CT_HOSTNAME}" \
        --volume="$PWD":/builds \
        --workdir /builds \
        "${DOCKER_IMAGE}"
      #sleep 30
      while_irods_wait "irods-test-${CI_COMMIT_SHORT_SHA}"
      i=0;
      for f in $RPM ci-test.sh test; do
          { docker exec --interactive "irods-test-${CI_COMMIT_SHORT_SHA}" test -e "$f"; } && \
            i=$((i+1))
      done
      if [ "${i:-0}" -lt 3 ]; then
        docker cp "$CI_PROJECT_DIR" "irods-test-${CI_COMMIT_SHORT_SHA}:${CI_PROJECT_DIR}"
      fi
      docker exec "$@" --interactive "irods-test-${CI_COMMIT_SHORT_SHA}" yum localinstall -y "$RPM"
      docker exec "$@" \
        --interactive \
        --env CI_DEBUG_TRACE="${CI_DEBUG_TRACE}" \
        --user user1 \
        "irods-test-${CI_COMMIT_SHORT_SHA}" /builds/ci-test.sh
      printf "making sure container '%s' is removed ... " "${NAME}-test"
      docker rm -f "irods-test-${CI_COMMIT_SHORT_SHA}" 2>/dev/null || true
      echo
      ;;
    publish)
      set_irods_ver
      set_rpm_file
      PKG_VER="$(./pkg-version.sh "$RPM")"
      docker run "$@" \
        --rm \
        --entrypoint '/bin/bash' \
        --env TOKEN="${TOKEN}" \
        --env IRODS_VERSION="${IRODS_VERSION}" \
        --env CI_DEBUG_TRACE="${CI_DEBUG_TRACE}" \
        --volume="$PWD":/builds \
        --workdir=/builds \
        "${DOCKER_IMAGE}" \
        ./gitlab-api-pkg.sh "$RPM" "$PKG_VER" |
        grep -q "201 Created" && echo "OK: $RPM" || exit 1
      ;;
    *)
      if ! docker ps -a --format='{{.Image}} {{.Names}}' | grep -Eq "$NAME" || [ "${FORCE:-0}" -eq 1 ]; then
        if [ "${FORCE:-0}" -eq 1 ]; then
          printf "Making sure existing container is removed first ... "
          docker rm -f -v "$NAME"
        fi
        printf "Starting irods container ... "
        docker run "$@" \
          --detach \
          --hostname "${CT_HOSTNAME}" \
          --name "${NAME}" \
          --volume="$PWD":/builds \
          "${DOCKER_IMAGE}" 
      else
        echo "Container '$NAME' already exists. To remove it, use: 'FORCE=1 ${SCRIPT}'"
      fi
      ;;
  esac
fi
#set +x
