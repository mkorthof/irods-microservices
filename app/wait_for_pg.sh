#!/bin/bash

# wait for pg service
RETRIES=10
_PG_HOST=${PG_HOST:-localhost}
_PG_USER=${PG_USER:-irods}
_PG_DATABASE=${PG_DATABASE:-ICAT}
_IRODS_DB_PASSWORD=${IRODS_DB_PASSWORD:-irods}

res=1
set +e
while [ $res != 0 ]; do
    echo "attempt to connect to database"
    PGPASSWORD=$_IRODS_DB_PASSWORD psql --user "$_PG_USER" --host="$_PG_HOST" --command='\q' "$_PG_DATABASE"
    res=$?
    RETRIES=$(( RETRIES - 1 ))
    if [ $res != 0 ]; then
        if [ $RETRIES == 0 ]; then
            echo "cannot connect to database"
            exit 8
        fi
        echo "failed: trying again after 5s, $RETRIES attempts left"
    else
        echo "connection to postgres database successful"
    fi
    sleep 5;
done
set -e