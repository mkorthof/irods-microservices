#!/bin/bash

if [ -n "$IRODS_ZONE" ]
then
    sed -i "s/tempZone/$IRODS_ZONE/g" /home/user2/.irods/irods_environment.json
    sed -i "s/tempZone/$IRODS_ZONE/g" /home/user1/.irods/irods_environment.json
fi

sleep infinity
