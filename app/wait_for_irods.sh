#!/bin/bash

# wait for irods service
RETRIES=10
res=1
set +e
while [[ $res -ne 0 ]]; do
    out="$(curl -4sS --stderr - localhost:1247)"
    res=$?
    echo "$out" | sed -e 's/^<\(.*\)>\(.*\)<.*/  \1: \2/g' -e '/^<\/.*/d' -e 's/^.*<\(.*\)>$/\1/'
    if [ $res != 0 ]; then
        RETRIES=$((RETRIES - 1))
        if [ $RETRIES == 0 ]; then
            echo "cannot connect to icat"
            cat /app/setup_irods.log
            exit 8
        fi
        echo "failed: trying again after 10s, $RETRIES attempts left"
        sleep 10
    fi
done
echo "successfully connected to icat"
set -e