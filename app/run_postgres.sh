#!/bin/bash

export PGDATA=/var/lib/pgsql/data
/usr/bin/pg_ctl start -D /var/lib/pgsql/data -o "-p 5432" -w -t 300
echo >/dev/tcp/localhost/5432 && echo "port 5432/tcp - accepting connections"
/usr/bin/pg_ctl status
tail -f /var/lib/pgsql/data/*log/postgresql-"$(date +%a)".log