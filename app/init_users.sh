#!/bin/bash

set -e
#set -x

_USER1_PASSWORD=${USER1_PASSWORD:-user1}
_USER2_PASSWORD=${USER2_PASSWORD:-user2}

for user in user1 user2; do
    # shellcheck disable=SC2001
    pvar="_$( echo $user | sed 's/./\U&/g' )_PASSWORD"
    if [ "$( gosu irods iadmin lu | grep -c '^'$user'#' )" == "0" ]; then
        gosu irods iadmin mkuser $user rodsuser
        gosu irods iadmin moduser $user password "${!pvar}"
    fi
    echo "${!pvar}" | gosu "$user" iinit >/dev/null && echo "added '$user'" || echo "could not add '$user'"
done
