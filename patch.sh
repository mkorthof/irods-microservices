#!/bin/sh

# Patch CMakeLists: 'include' DetectOS module (and replace file for WUR)

if [ -n "$1" ] && [ -n "$2" ]; then
  REPO_DIR="$1"
  CI_SERVER_URL="$2"
fi
if [ -z "$REPO_DIR" ]; then
  exit 1
fi
if [ "$CI_DEBUG_TRACE" = "true" ]; then
  set -x
fi
if [ "${REPO_DIR}" = "wur-microservices" ]; then
  curl -sSL \
    "${CI_SERVER_URL:-"https://gitlab.com"}/mkorthof/irods-wur-microservices-cmake/-/raw/master/CMakeLists.txt" \
    -o "${REPO_DIR}/CMakeLists.txt"
fi
# shellcheck disable=SC2016
sed -i 's|^include(${CMAKE_MODULE_PATH}/DetectOS.cmake)|include(DetectOS)|' "${REPO_DIR}/CMakeLists.txt" || true
set +x
