#!/bin/bash
cmd=$1
shift
#set -x

# 4_3_x: use python3

_RODS_PASSWORD=${RODS_PASSWORD:-rods}
_IRODS_DB_PASSWORD=${IRODS_DB_PASSWORD:-irods}
_IRODS_ZONE=${IRODS_ZONE:-tempZone}

# start pg
pgrep -a -n /usr/bin/postgres || gosu postgres /app/run_postgres.sh &
PG_HOST="icat.irods" gosu postgres /app/wait_for_pg.sh

# start irods
if [ -e /var/lib/irods/iRODS/Vault/home ] && [ "${FORCE_START:-0}" -eq 0 ]; then
  echo "irods already installed"
  pgrep -a -n irodsServer || gosu irods python3 /var/lib/irods/scripts/irods_control.py start
else
  cp /app/setup_answers.txt /app/setup_answers.txt.bak
  sed -i s/__RODS_PASSWORD__/"${_RODS_PASSWORD}"/g /app/setup_answers.txt
  sed -i s/__IRODS_DB_PASSWORD__/"${_IRODS_DB_PASSWORD}"/g /app/setup_answers.txt
  sed -i s/__IRODS_ZONE__/"${_IRODS_ZONE}"/g /app/setup_answers.txt
  sed -i 's/^__DATABASE_HOST__/icat.irods/g' /app/setup_answers.txt
  python3 /var/lib/irods/scripts/setup_irods.py </app/setup_answers.txt &>/app/setup_irods.log &&
    { pgrep -a -n irodsServer || gosu irods python3 /var/lib/irods/scripts/irods_control.py start; } &
fi

# set zone
if [ "$_IRODS_ZONE" != tempZone ]; then
  sed -i "s/tempZone/$_IRODS_ZONE/g" /root/.irods/irods_environment.json
  sed -i "s/tempZone/$_IRODS_ZONE/g" /home/user1/.irods/irods_environment.json
  sed -i "s/tempZone/$_IRODS_ZONE/g" /home/user2/.irods/irods_environment.json
fi

echo "waiting for irods ... "
/app/wait_for_irods.sh

echo "init users ... "
echo "$_RODS_PASSWORD" | gosu irods iinit 2>/dev/null && echo "added 'irods'" || echo "could not add 'irods'"
/app/init_users.sh

chmod a+r /var/lib/irods/version.json

echo "done setting up irods"

# execute command
if [ -z "$cmd" ]; then
  gosu sleep infinity
else
  gosu irods "$cmd" "$@"
fi
