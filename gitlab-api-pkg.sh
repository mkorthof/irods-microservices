#!/bin/bash

########################################
# GITLAB PACKAGES API - WRAPPER SCRIPT #
########################################
#
# USAGE:
#   - publish package  ./gitlab-api-pkg.sh <file_name> <package_version>
#   - list package(s)  ./gitlab-api-pkg.sh
#     (to list run manually without args and outside CI)
#
# DOCS:
#   - https://docs.gitlab.com/ee/user/packages/generic_packages/index.html
#   - GitLab version format is 1.2.3-1, regex: [0-9.-]
#
# REQUIRES TOKEN:
#   CI_JOB_TOKEN or PAT with scope 'api, write_repository'
#
# API EXAMPLES:
#   curl --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/:id/packages"
#   curl --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/groups/:id/packages?exclude_subgroups=false"
#   curl --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/:id/packages/:package_id"
#   curl --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/:id/packages/4/package_files"
#   curl --request DELETE --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/:id/packages/:package_id"
#   curl --request DELETE --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/:id/packages/:package_id/package_files/:package_file_id"
#

if [ "$CI_DEBUG_TRACE" = "true" ]; then
  set -x
fi
CATEGORY="projects"
# STATUS="default" # set to "default" or "hidden"
if [ -z "$FILE_NAME" ] && [ -n "$1" ]; then
  FILE_NAME="$1"
fi
if [ -z "$PACKAGE_VERSION" ] && [ -n "$2" ]; then
  PACKAGE_VERSION="$2"
fi

# Check if we're running from CI

if [ "$CI" ]; then
  GITLAB="$CI_API_V4_URL"
  TOKEN="JOB-TOKEN: $CI_JOB_TOKEN"
else
  GITLAB="https://gitlab.com/api/v4"
  if [ -z "$TOKEN" ]; then
    TOKEN="PRIVATE-TOKEN: $(cat ~/.secret/gitlab)"
  fi
  #CI_PROJECT_NAME="irods-example-microservices"
fi

OUTPUT=$(command -v jq || echo cat)

# Call API with curl to upload and publish package or list pkgs

if [ -f "$FILE_NAME" ] && [ -n "$PACKAGE_VERSION" ]; then
  /usr/bin/curl --silent \
    --header "$TOKEN" \
    --upload-file "$FILE_NAME" \
    "${GITLAB}/${CATEGORY}/${CI_PROJECT_ID}/packages/generic/${PACKAGE_VERSION:-"my_pkg/0"}/${FILE_NAME}?status=${STATUS:-default}"
else
  if [ -z "$CI" ]; then
    /usr/bin/curl --silent --header "$TOKEN" "${GITLAB}/${CATEGORY}/${CI_PROJECT_ID}/packages" | $OUTPUT
    exit 0
  fi
  exit 1
fi
set +x
