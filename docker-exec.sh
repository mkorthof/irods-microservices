#!/bin/bash

# Run command in container, default is 'irods'

if [ -z "$*" ]; then
  echo "missing arguments, try e.g. './docker-exec.sh bash'"
  exit 1
fi

if printf "%s" "$1" | grep -Eq -- '^\-'; then
  DOCKER_OPTS=("${@:1:2}")
  CT_CMD=("${@:3}")
else
  CT_CMD=("$@")
fi

FILTER="name=^${CT_CMD[0]}$"
if printf "%s" "${CT_CMD[0]}" | grep -E "[0-9a-f]{12}"; then
  FILTER="id=${CT_CMD[0]}"
fi
ID="$(docker ps -l --format '{{.ID}}' --filter "$FILTER")"
if [ -n "$ID" ]; then
  docker exec -it "${DOCKER_OPTS[@]}" "$ID" "${CT_CMD[@]:1}"
else
  echo "using default 'irods' container.."
  docker exec -it "${DOCKER_OPTS[@]}" irods "${CT_CMD[@]}"
fi
