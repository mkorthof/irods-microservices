#!/bin/sh

# Test irods microservices

if [ "$CI_DEBUG_TRACE" = "true" ]; then
  set -x
fi

func_scripts() {
  if [ -d "${1}" ]; then
    for i in "${1}"/*.sh; do
      if [ -x "${i}" ]; then
        "./${i}" && echo "OK: $i" || exit 1
      else
        echo "SKIP: $i  (not executable)"
      fi
    done
  fi
}

func_irules() {
  if [ -d "${1}" ]; then
    for i in "${1}"/*.r; do
      irule -F "$i" && echo "OK: $i" || exit 1
    done
  fi
}

# first run all executable scripts in test/pre-scripts.d dir
func_scripts test/pre-scripts.d

# run irods rules in test/irules.d dir and any tests included in src repo
func_irules test/irods-rules.d
func_irules "${REPO_DIR}/tests"

# run post scripts (e.g. cleanup) from test/post-scripts.d
func_scripts test/post-scripts.d

set +x
