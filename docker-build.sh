#!/bin/bash

################################################################################
# Build local docker image
# Uses default variables from .gitlab-ci.yml, needs 'yq' installed
# Example: 'IRODS_VERSION=4.2.11 ./docker-build.sh'  (overrides defaults)
################################################################################

# Set any 'local' vars below to override both defaults and env vars, e.g.
#   IRODS_VERSION="4.2.0"
#   INSTALL_ALL_IRODS_EXTERNALS=0
# ..or to use different base os:
#   RHEL_OS_NAME=almalinux
#   RHEL_OS_VERSION=8.7
# ..or centos8 from redhats registry:
#   RHEL_OS_NAME=quay.io/centos/centos
#   RHEL_OS_VERSION=stream8


SCRIPT="./$(basename "$0")"

if echo "$@" | grep -Eq -- "(-h| help)"; then
  cat <<-_EOF_

  Build docker image

  Uses default variables from .gitlab-ci.yml

  To override:
    RHEL_OS_NAME=almalinux RHEL_OS_VERSION=8 ${SCRIPT}

_EOF_
  exit 0
fi

func_source_vars() {
  # shellcheck disable=SC1090
  source ./.vars.$$
  if [ "$DEBUG" -gt 1 ]; then
    cat .vars.$$
  fi
  rm .vars.$$
}

# Make sure temp .vars.XXX get cleaned up
trap 'rm -f .vars.$$' EXIT HUP INT TERM
./gitlab-ci-vars.sh --ci --global > .vars.$$ && func_source_vars
./gitlab-ci-vars.sh --workflow > .vars.$$ && func_source_vars

#set -x
if [ -z "${RHEL_OS_NAME}" ] || [ -z "${RHEL_OS_VERSION}" ]; then
  echo "ERROR: missing 'RHEL_OS_NAME' and/or 'RHEL_OS_VERSION'"
  exit 1
fi
if [ -z "${IRODS_VERSION}" ]; then
  #IRODS_VERSION="4.3.0"
  echo "ERROR: missing 'IRODS_VERSION'"
  exit 1
fi
if [ -z "${DOCKER_TAG}" ]; then
  echo "WARNING: missing 'DOCKER_TAG', using default 'full'"
  DOCKER_TAG="full"
  export DOCKER_TAG
fi

# remove registry
DOCKER_IMAGE="${DOCKER_IMAGE##*/}"

if [ "${DEBUG:-0}" -ge 1 ]; then
  echo "DEBUG: RHEL_OS_NAME=$RHEL_OS_NAME"
  echo "DEBUG: RHEL_OS_VERSION=$RHEL_OS_VERSION"
  echo "DEBUG: DOCKER_IMAGE=${DOCKER_IMAGE}"
  echo "DEBUG: DOCKER_TAG=$DOCKER_TAG"
  echo "DEBUG: IRODS_VERSION=$IRODS_VERSION"
  echo "DEBUG: INSTALL_ALL_IRODS_EXTERNALS=$INSTALL_ALL_IRODS_EXTERNALS"
  #echo "DEBUG: INSTALL_ALL_IRODS_PLUGINS=$INSTALL_ALL_IRODS_PLUGINS"
  #echo "DEBUG: INSTALL_DEVTOOLS=$INSTALL_DEVTOOLS"
  #echo "DEBUG: INSTALL_PYTHON=$INSTALL_PYTHON"
  #echo "DEBUG: INSTALL_OPENSTACK_CLIENTS=$INSTALL_OPENSTACK_CLIENTS"
  #echo "DEBUG: INSTALL_CATCH=$INSTALL_CATCH"
  echo "DEBUG: IRODS_EXTERNALS=$IRODS_EXTERNALS"
  if [ "${DEBUG:-0}" -ge 2 ]; then
    exit
  fi
fi

# default is not to cache
DOCKER_CACHE="--no-cache"
if [ "${CACHE:-0}" -eq 1 ]; then
  docker pull "${DOCKER_IMAGE}:${DOCKER_TAG}" || true
  DOCKER_CACHE="--cache-from "${DOCKER_IMAGE}:${DOCKER_TAG}""
fi

# shellcheck disable=SC2068,SC2086
docker build "$@" \
  $DOCKER_CACHE \
  --tag "${DOCKER_IMAGE}" \
  --build-arg RHEL_OS_NAME="${RHEL_OS_NAME}" \
  --build-arg RHEL_OS_VERSION="${RHEL_OS_VERSION}" \
  --build-arg IRODS_VERSION="${IRODS_VERSION}" \
  --build-arg INSTALL_ALL_IRODS_EXTERNALS="${INSTALL_ALL_IRODS_EXTERNALS:-1}" \
  --build-arg INSTALL_ALL_IRODS_PLUGINS="${INSTALL_ALL_IRODS_PLUGINS:-0}" \
  --build-arg INSTALL_DEVTOOLS="${INSTALL_DEVTOOLS:-0}" \
  --build-arg INSTALL_PYTHON="${INSTALL_PYTHON:-0}" \
  --build-arg INSTALL_OPENSTACK_CLIENTS="${INSTALL_OPENSTACK_CLIENTS:-0}" \
  --build-arg INSTALL_CATCH="${INSTALL_CATCH:-0}" \
  --build-arg IRODS_EXTERNALS="${IRODS_EXTERNALS:-""}" \
  --build-arg http_proxy="${http_proxy:-$HTTP_PROXY}" \
  .
docker tag "${DOCKER_IMAGE}" "${DOCKER_IMAGE}:${DOCKER_TAG}"
docker tag "${DOCKER_IMAGE}" "${DOCKER_IMAGE}:latest"
#set +x
