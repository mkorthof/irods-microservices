#!/bin/sh

#IRODS_START=1

PYVER="$(command -v python python3 2>/dev/null | tail -1)"
PYBIN="${PYVER:-python}"
IRODS_START="${IRODS_START:-0}"
IRODS_VERSION="$(
$PYBIN -c "import json, os;
ver_path = '/var/lib/irods/VERSION.json' if os.path.exists('/var/lib/irods/VERSION.json') else '/var/lib/irods/version.json';
print(str(json.load(open(ver_path)).get('irods_version', '')))"
)"
IRODS_CONFIG="setup_irods_json/setup_irods_${IRODS_VERSION:-"4.3.0"}.json"

$PYBIN /var/lib/irods/scripts/setup_irods.py --verbose --json_configuration_file="${IRODS_CONFIG}" && \
    { test "${IRODS_START}" -eq 1 && { pgrep -a -n irodsServer || $PYBIN /var/lib/irods/scripts/irods_control.py start; }; }
