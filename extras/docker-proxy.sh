#!/bin/sh

# Squid proxy for caching (rpm) files during docker build

SCRIPTDIR="$(dirname "$(readlink -f -- "$0")")"
if [ ! -s "${SCRIPTDIR}/proxy/docker.conf" ]; then
  echo "ERROR: docker.conf missing"
  exit 1
fi

_id="$(docker ps -a --filter="name=squid-container" --format '{{.ID}}')"
if [ -n "$_id" ]; then
  docker rm -f squid-container
fi
docker run \
  --detach \
  --name squid-container \
  --env TZ=UTC \
  --publish 3128:3128 \
  --volume "${SCRIPTDIR}"/proxy/docker.conf:/etc/squid/conf.d/docker.conf \
  --volume "${SCRIPTDIR}"/proxy/logs:/var/log/squid \
  --volume "${SCRIPTDIR}"/proxy/data:/var/spool/squid \
  --rm \
  ubuntu/squid
_ip="$(docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' squid-container)"
if [ -n  "$_ip" ]; then
  echo "Use proxy by setting 'http_proxy=$_ip:3128'"
else
  echo "Could not get ip address, try: 'docker inspect squid-container'"
fi
