################################################################################
# Create RHEL based iRODS image to build, dev and test microservices
################################################################################
# Default OS is centos7, other RHEL distros should also work
ARG RHEL_OS_NAME=centos
ARG RHEL_OS_VERSION=7
FROM ${RHEL_OS_NAME}:${RHEL_OS_VERSION}
ARG IRODS_VERSION=*
ARG INSTALL_ALL_IRODS_EXTERNALS=1
ARG INSTALL_ALL_IRODS_PLUGINS=0
ARG INSTALL_DEVTOOLS=0
ARG INSTALL_PYTHON=0
ARG INSTALL_OPENSTACK_CLIENTS=0
ARG INSTALL_CATCH=0
ARG IRODS_EXTERNALS

################################################################################
# Setup base, pkgs, extra repos, pgsql
################################################################################
# Install gosu binary
# hadolint ignore=DL3033,DL3013,SC1091,DL3040,DL3041
RUN curl -sSL https://github.com/tianon/gosu/releases/download/1.14/gosu-amd64 -o /usr/local/bin/gosu && \
    curl -sSL -O https://github.com/tianon/gosu/releases/download/1.14/gosu-amd64.asc && \
    gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && \
    gpg --batch --verify gosu-amd64.asc /usr/local/bin/gosu && \
    chmod +x /usr/local/bin/gosu && \
	  gosu nobody true && \
# Enable yum proxy if set
    test -n "$http_proxy" && echo "proxy=${http_proxy}" >>/etc/yum.conf; \
# CI: Skip a few slow yum mirrors
    test "$CI" && echo "exclude=tripadvisor.com, cmich.edu, nodesdirect.com, velocihost.net" >> \
      /etc/yum/pluginconf.d/fastestmirror.conf; \
    microdnf install -y yum 2>/dev/null ;\
    yum update -y && \
# Enable repos: epel, powertools (rhel8), crb (rhel9)
    yum install -y epel-release; \
    . /etc/os-release && \
      CHECK_VERSION="${VERSION_ID%%.*}"; \
      if [ "${CHECK_VERSION:-0}" -ge 8 ]; then \
        yum install -y dnf-plugins-core; \
      fi; \
      if [ "${CHECK_VERSION:-0}" -eq 8 ]; then \
        yum config-manager --set-enabled powertools; \
        command -v /usr/bin/crb >/dev/null 2>&1 && /usr/bin/crb enable; \
      fi; \
      if [ "${CHECK_VERSION:-0}" -ge 9 ]; then \
        yum config-manager --set-enabled crb; \
      fi; \
    yum update -y && \
    yum install -y \
# Install packages: PostgreSQL
      authd \
      postgresql \
      postgresql-odbc\
      postgresql-server \
# Install packages: required libs and tools
      libstdc++-static \
      libcurl-devel \
      ca-certificates \
      chrpath \
      #coreutils \
      gcc-c++ \
      git \
      gnupg \
      make \
      libtool \
      less \
      lsof \
      openssl \
      openssl-devel \
      procps-ng \
      python36 \
      python3-pip \
      rpm-build \
      rpm-devel \
      rpmlint \
      rpmdevtools \
      rpmrebuild \
      rsync \
      rsyslog \
      s3cmd \
      unixODBC \
      wget \
      which; \
# Install packages: extra libs
    yum install -y \
      libarchive-devel \
      jansson-devel \
      librabbitmq-devel \
      libuuid-devel; \
# Install dev tools [optional]
    if [ ${INSTALL_DEVTOOLS:-0} -eq 1 ]; then \
      yum install -y \
        autoconf \
        automake \
        boost-devel \
        cmake \
        diffutils \
        patch \
        fuse-libs \
        gcc \
        help2man \
        perl-JSON \
        pam-devel \
        unixODBC-devel; \
      fi; \
# Make sure jsonschema is installed, its required by irods installer
    yum install -y python3-jsonschema 2>/dev/null; \
    if ! python3 -c 'import jsonschema' 2>/dev/null; then \
      python3 -m pip install --no-cache-dir jsonschema 2>/dev/null; \
    fi; \
    yum clean all

################################################################################
# Python, OpenStack clients, Catch22 and rpmbuild
################################################################################
# Always upgrade pip3 and install iRODS Client
# hadolint ignore=DL3013,DL3033
RUN python3 -m pip install --no-cache-dir --upgrade pip 2>/dev/null && \
    python3 -m pip install --no-cache-dir \
      python-irodsclient \
      pyodbc 2>/dev/null && \
# Install extra modules [optional]
    if [ ${INSTALL_PYTHON:-0} -eq 1 ]; then \
      yum update -y && \
      yum install -y \
        python2 \
        python2-devel \
        python2-psutil \
        python2-pip \
        python38 \
        python39 \
        python2-requests \
        python3-breathe \
        python3-distro \
        python3-devel \
        python3-flask \
        python3-flask-restful \
        python3-requests \
        python3-psutil \
        python3-pytest \
        python3-sphinx && \
      yum clean all && \
      python2 -m pip install --no-cache-dir \
        python-irodsclient \
        behave \
        distro \
        jsonschema \
        flask==1.1.4 \
        flask_restful==0.3.9 \
        markupsafe==1.1.1 2>/dev/null && \
      python3 -m pip install --no-cache-dir \
        setuptools_rust \
        xmlrunner 2>/dev/null; \
    fi

# OpenStack clients [optional]
# hadolint ignore=DL3013
RUN if [ ${INSTALL_OPENSTACK_CLIENTS:-0} -eq 1 ]; then \
  python -m pip install --no-cache-dir \
    python-swiftclient \
    python-keystoneclient \
    python-openstackclient 2>/dev/null; \
fi

# Install Catch2 (unit test c++) [optional]
# hadolint ignore=DL3003
RUN if [ ${INSTALL_CATCH:-0} -eq 1 ]; then \
      cd /opt && \
      curl -sSL -O https://github.com/catchorg/Catch2/archive/v2.9.2.tar.gz && \
      tar -xvf v2.9.2.tar.gz && \
      mkdir /opt/include && \
      ln -s /opt/Catch2-2.9.2/include/ /opt/include/catch2 && \
      rm -fr Catch2-2.9.2; \
    fi

# Setup rpmbuild
RUN useradd -r -m -d /home/rpmbuild -s /bin/bash rpmbuild
COPY --chown=rpmbuild:rpmbuild app/build_rpm.sh /home/rpmbuild/build_rpm.sh
RUN rpmdev-setuptree
USER rpmbuild 
RUN chown -R rpmbuild: /home/rpmbuild

################################################################################
# Install iRODS
################################################################################
# Repos and packages
# hadolint ignore=DL3002
USER root
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# hadolint ignore=SC2086
RUN yum update -y && \
    rpm --import https://packages.irods.org/irods-signing-key.asc && \
    curl -s https://packages.irods.org/renci-irods.yum.repo | \
      tee /etc/yum.repos.d/renci-irods.yum.repo && \
    rpm --import https://core-dev.irods.org/irods-core-dev-signing-key.asc && \
    curl -s https://core-dev.irods.org/renci-irods-core-dev.yum.repo | \
      tee /etc/yum.repos.d/renci-irods-core-dev.yum.repo && \
    yum install -y \
      irods-devel-${IRODS_VERSION:-*} \
      irods-server-${IRODS_VERSION:-*} \
      irods-database-plugin-postgres-${IRODS_VERSION:-*} \
      irods-icommands-${IRODS_VERSION:-*} \
      irods-runtime-${IRODS_VERSION:-*} && \
# Always install Python Rule Engine
    yum install -y "irods-rule-engine-plugin-python-${IRODS_VERSION:-*}*"; \
# Install All iRODS Rule Engine Plugins [optional]
    if [ ${INSTALL_IRODS_PLUGINS:-0} -eq 1 ]; then \
      yum install -y "irods-rule-engine-plugin-${IRODS_VERSION:-*}*"; \
    fi && \
    yum clean all

COPY app/odbcinst.ini /etc/odbcinst.ini

# Scripts
COPY app/irods_environment.json /root/.irods/irods_environment.json
COPY app/wait_for_pg.sh /app/wait_for_pg.sh
COPY app/init_users.sh /app/init_users.sh
COPY app/wait_for_irods.sh /app/wait_for_irods.sh

# Create user1 and user2
RUN useradd -r -m -d /home/user1 -s /bin/bash user1 && \
    useradd -r -m -d /home/user2 -s /bin/bash user2
COPY --chown=user1:user1 app/irods_environment.json /home/user1/.irods/irods_environment.json
COPY --chown=user2:user2 app/irods_environment.json /home/user2/.irods/irods_environment.json

# Setup preparation
COPY setup /tmp/setup
RUN _ver_dir="$( echo $IRODS_VERSION | sed -r -e 's/\./_/g' -e 's/[0-9]{1,2}$/x/' )"; \ 
    cp "/tmp/setup/${_ver_dir}/setup_answers.txt" /app/setup_answers.txt; \ 
    cp "/tmp/setup/${_ver_dir}/setup_irods.sh" /app/setup_irods.sh; \
    cp "/tmp/setup/${_ver_dir}/restart_irods.sh" /app/restart_irods.sh; \
    cp "/tmp/setup/${_ver_dir}/config.mk" /opt/config.mk; \
    sed -i 's/"irods_user_name": "rods"/"irods_user_name": "user1"/' /home/user1/.irods/irods_environment.json; \
    sed -i 's/"irods_user_name": "rods"/"irods_user_name": "user2"/' /home/user2/.irods/irods_environment.json
RUN groupadd -r irods && \
    useradd -r -d /var/lib/irods -M -s /bin/bash -g irods -c 'iRODS Administrator' -p '!' irods

################################################################################
# iRODS externals: all or minimal [optional]
################################################################################
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# hadolint ignore=SC2086,DL3033
RUN if [ ${INSTALL_ALL_IRODS_EXTERNALS:-1} -eq 1 ]; then \
      yum install -y 'irods-externals*'; \
    else \
# Install latest irods-externals versions only
      yum list -q all | sort -k1 -r -V >/tmp/yum.list && \
      for i in $IRODS_EXTERNALS; do \
        _out+="$(grep -m1 -Pow '^irods-externals-'"$i"'[^ ]*' /tmp/yum.list) "; \
      done; \
      test -n "${_out// /}" && \
      yum install -y $_out; \
      rm -f /tmp/yum.list; \
    fi; \
    yum clean all

################################################################################
# Configure PostgreSQL
################################################################################
COPY --chown=postgres:postgres app/run_postgres.sh /app/run_postgres.sh
USER postgres
RUN /usr/bin/initdb --pgdata="/var/lib/pgsql/data" --auth=ident && \
  for i in "127.0.0.1/32" "172.16.0.0/12" "fe80::/10"; do \
    echo "host all  all    $i md5" >> /var/lib/pgsql/data/pg_hba.conf; \
  done && \
  echo "listen_addresses='*'" >> /var/lib/pgsql/data/postgresql.conf && \
  /usr/bin/pg_ctl start -D /var/lib/pgsql/data -o "-p 5432" -w -t 300 && \
  /usr/bin/psql --command "CREATE USER docker WITH SUPERUSER PASSWORD 'docker';" && \
  /usr/bin/createdb -O docker docker && \
  /usr/bin/psql --command "CREATE USER irods WITH PASSWORD 'irods';" && \
  /usr/bin/psql --command 'CREATE DATABASE "ICAT";' && \
  /usr/bin/psql --command 'GRANT ALL PRIVILEGES ON DATABASE "ICAT" TO irods;' && \
  /usr/bin/pg_ctl stop -D /var/lib/pgsql/data -m fast

# hadolint ignore=DL3002
USER root
ENTRYPOINT [ "/app/setup_irods.sh" ]
CMD [ "sleep", "infinity" ]
