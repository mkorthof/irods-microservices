#!/bin/sh

# Build irods microservices, uses irods-externals

if [ "$CI_DEBUG_TRACE" = "true" ]; then
  set -x
fi
_IRODS_VERSION=$(sed -n 's/^set(IRODS_VERSION "\([0-9.]\+\)")$/\1/p' CMakeLists.txt)
if [ "$_IRODS_VERSION" != "${IRODS_VERSION:-}" ]; then
  echo "WARNING: irods version '$_IRODS_VERSION' in CMakeLists.txt does not match '$IRODS_VERSION' ci var, continueing anyways.."
  #sed 's/^set(IRODS_VERSION "[0-9.]\+")/set(IRODS_VERSION "'"$_IRODS_VERSION"'")/' CMakeLists.txt
fi
CMAKE_DIR="$(find /opt/irods-externals/cmake*/bin -maxdepth 0 -printf '%p\n' | sort -r --version-sort | head -1)"
if [ -z "$CMAKE_DIR" ]; then
  echo "WARNING: cmake bin dir not found, continueing anyways.."
fi
make clean >/dev/null 2>&1 || true
rm -rf -- cmake _CPack_Packages CMakeFiles CMakeCache.txt install_manifest.txt Makefile ./*.cmake ./libmsi*.so ./*.rpm || true
PATH=$CMAKE_DIR:$PATH \
  cmake . && \
  make && \
  QA_RPATHS=0x0002 make package
set +x
