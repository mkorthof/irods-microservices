#!/bin/sh

set -x

# prepare testdir and files
mkdir /tmp/testDir
echo "Text File 1: This is a test" >"/tmp/testDir/test1.txt"
echo "Text File 2: This is another test" >"/tmp/testDir/test2.txt"
dd if=/dev/urandom of=/tmp/testDir/test3.bin bs=1M count=1 status=none
# md5sum /tmp/testDir/test3.bin > /tmp/testDir/test3.bin.md5
iput -r /tmp/testDir testDir1
iput -r /tmp/testDir testDir2
