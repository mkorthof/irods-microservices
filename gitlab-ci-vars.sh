#!/bin/bash

# Gets gitlab ci variables and outputs as 'export VAR=value'
# - Usage: ./gitlab-ci.sh <--ci|--global|--workflow>

# Predefined CI/CD vars
if echo "$@" | grep -Eiq -- "\--ci"; then
  COMMON_CI_VARS='
    CI_DEFAULT_BRANCH=main
    CI_COMMIT_BRANCH=main
    CI_PROJECT_DIR=/builds
    CI_REGISTRY=""
    CI_REGISTRY_IMAGE=""
  '
  # disabled
  #CI_PROJECT_ID="123"
  #CI_JOB_TOKEN="ABC123DEF"
  #CI_REGISTRY="localhost:5000"
  #CI_REGISTRY_IMAGE="localhost:5000"
  #CI_REGISTRY_IMAGE="registry.gitlab.com/example/irods-microservices"
  for i in $COMMON_CI_VARS; do
    echo 'test -z "$'"${i%%=*}"'" && export '"$i"''
  done
fi

# Global vars from .gitlab-ci.yml
if echo "$@" | grep -Eiq -- "\--global"; then
  if command -v yq >/dev/null 2>&1 && [ -s .gitlab-ci.yml ]; then
    yq -r '.variables | to_entries[] | "test -z \"$" + .key + "\" && export " + .key + "=\"" + .value + "\""' .gitlab-ci.yml
  fi
fi

# Convert vars in workflow rules to bash conditionals
if echo "$@" | grep -Eiq -- "\--workflow"; then
  if command -v yq >/dev/null 2>&1 && [ -s .gitlab-ci.yml ]; then
    yq -r '.workflow.rules[] | "if [[ \(.if|gsub("/";"")) ]]; then
      \(select(.variables!=null) | .variables | to_entries |
      map("test -z \"$" + .key + "\" && export " + .key +"="+.value | [strings? // .[]] |join(",")) | join("; "));
    fi"' .gitlab-ci.yml
  fi
fi
