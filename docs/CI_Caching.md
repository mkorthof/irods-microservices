# CI Cache

Meant for e.g. python or npm module caches.

https://docs.gitlab.com/ee/ci/caching/

Example:

Share test dir from job1 to job2

```
job1:
  script:
    git clone irods-microservices       # repo has 'test' dir
  cache:
    key: tests-${CI_COMMIT_SHORT_SHA}
    paths:
      - ./*/tests/*.r
    policy: push

job2:
  cache:
    key: tests-${CI_COMMIT_SHORT_SHA}
    paths: 
      - test/irods-rules.d/
    policy: pull
```

(note that for files in this example, actually use "Artifacts" instead)

