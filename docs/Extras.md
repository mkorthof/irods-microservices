# Extras

Extra tidbits, not currently used in main repo.

See [extras](/extras) dir

## iRODS Setup with JSON

Unattended/automated installation using `setup_irods.py` with json file providing configuration settings.

Can be used as an alternative method instead of providing setup answers using stdin redirection.

- https://funinit.wordpress.com/2019/05/04/ansible-based-automated-deployment-of-irods-grid/
- https://github.com/irods/irods/blob/main/packaging/server_config.json.template
- https://github.com/spachika/irods-automation/blob/master/unattended_installation.json

### Examples

See included dir [extras/setup_irods_json](extras/setup_irods_json) and [setup_irods_json.sh](extras/setup_irods_json.sh)

### Config

Note that keys/values may differ per irods version

Create setup_irods.json from existing setup with `izonereport`:

- extract `"host_access_control_config"`, `"hosts_config"` and `"service_account_environment"`
- set irods keys `"negotiation_key"`, `"zone_key"` etc by replacing "XXXX..."
- add: `"default_resource_name: [ "demoResc" ]`
- add: `"admin_password": "MyPass123"`

### Run

`python /var/lib/irods/scripts/setup_irods.py --verbose --json_configuration_file=setup_irods.json`

Check if irods is started after setup finishes


## Packages and Caching

### Repos

Repo dirs for official irods rpm pkgs:

- https://packages.irods.org/yum/pool/centos7/x86_64/
- https://packages.irods.org/yum/pool/el8/

### Proxy

To cache pkgs during yum install, use Squid proxy in extras/ dir:

- run `./docker-proxy.sh`
- ip address of container should be shown (e.g. 172.17.0.3)
- set environment variable: `http_proxy=http://<ip_addr>:3182`
- run docker build
