# CI Services

Run postgres as service container instead of in same container as irods

Caveats:

- runs per job only, not shared
- needs privileged mode
- same as `docker run --link`
- hostname is same as image name ('postgres') or set with `alias`

Example:

```
job:
  image: centos7_irods
  services:
    - postgres:9.2                   # or 12.10 etc
  variables: 
    FF_NETWORK_PER_BUILD: "true"     # activate container-to-container (dind/docker run)
    POSTGRES_DB: ICAT
    POSTGRES_USER: irods
    POSTGRES_PASSWORD: irods
```

Local docker:

(for debugging)

```
docker run \
  --detach \
  --name service-postgres \
  --env POSTGRES_USER=irods \`
  --env POSTGRES_PASSWORD=rods \
  --env POSTGRES_DB=ICAT \
  postgres:9.2

docker run "$@" \
  --detach \
  --hostname "$HOSTNAME" \
  --name "${NAME}-test" \
  --volume="$PWD":/builds \
  --rm \
  --workdir /builds \
  --link=service-postgres:postgres \
  "${DOCKER_IMAGE}"
```

More details:

- https://docs.gitlab.com/ee/ci/services/
- https://gitlab.com/gitlab-examples/postgres/-/blob/master/.gitlab-ci.yml
